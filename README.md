# Worker Core
This is core class for worker RabbitMQ

### Installation
Require this package in your `composer.json` and update composer. This will download the package.
```php
{
    "require": {
        "eggdigital/egg-worker-core": "dev-master"
    },
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "eggdigital/egg-worker-core",
                "version": "dev-master",
                "source": {
                    "url": "https://bitbucket.org/jeurboy/egg-worker-core.git",
                    "type": "git",
                    "reference": "origin/master"
                },
                "autoload": {
                    "classmap": [
                        "src/classes",
                        "src/services"
                    ],
                    "psr-0": {
                        "EggDigital\\Worker\\": "src/"
                    }
                }
            }
        }
    ]
}
```

### Usage
You can copy this code or copy file `cli.php` and `WorkerProcess.php` in folder `template`
```php
// cli.php
<?php

// Import class WorkerProcess
require_once __DIR__ . '/WorkerProcess.php';

// Process the console arguments
$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['queue_name'] = strtolower($arg);
    }
}

// Setting your config server for RabbitMQ
$conf = [
    'rabbit' => [
        'servers' => [
            [
                'host'     => 'egg_activity_ms_rabbitmq1',
                'port'     => '5672',
                'username' => 'guest',
                'password' => 'guest'
            ]
        ]
    ]
];

if (!isset($arguments['queue_name'])) {
    echo "Please enter queue name\n";
    exit;
}

// Setting for queue name
$queue_name = $arguments['queue_name'];

// Build object WorkerProcess and send parameter of config
$worker_process = new WorkerProcess($conf);

// Start worker
$worker_process->startWorker($queue_name);
```

```php
// WorkerProcess.php
<?php

// Import core worker
use EggDigital\Worker\Classes\BaseWorker;

class WorkerProcess extends BaseWorker
{
    public function __construct($conf)
    {
        parent::__construct();
        
        // Setting config
        $this->conf_queue = $conf;
    }

    // You can edit callback
    public function setCallbackFn($job)
    {
        echo "Worker Running\n";
        sleep(3);
    }
}
```

### Running
You can running with command
```sh
$ php cli.php queue_name
```
