<?php

require_once __DIR__ . '/WorkerProcess.php';

// Process the console arguments
$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['queue_name'] = strtolower($arg);
    }
}

$conf = [
    'rabbit' => [
        'servers' => [
            [
                'host'     => 'egg_activity_ms_rabbitmq1',
                'port'     => '5672',
                'username' => 'guest',
                'password' => 'guest'
            ]
        ]
    ]
];

if (!isset($arguments['queue_name'])) {
    echo "Please enter queue name\n";
    exit;
}

$queue_name = $arguments['queue_name'];

$worker_process = new WorkerProcess($conf);

$worker_process->startWorker($queue_name);
