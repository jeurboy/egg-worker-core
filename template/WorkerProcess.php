<?php

use EggDigital\Worker\Classes\BaseWorker;

class WorkerProcess extends BaseWorker
{
    public function __construct($conf)
    {
        parent::__construct();
        
        $this->conf_queue = $conf;
    }

    public function setCallbackFn($job)
    {
        echo "Worker Running\n";
        sleep(3);
    }
}
