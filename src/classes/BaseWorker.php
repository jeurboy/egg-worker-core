<?php

namespace EggDigital\Worker\Classes;

use EggDigital\Worker\Services\CmdOutputServices;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class BaseWorker
{
    protected $conf_queue;
	protected $cmd_output_services;
	private $connection;
	private $channel;
    private $tps;

    public function __construct()
    {
        $this->cmd_output_services = new CmdOutputServices();
        
        // Set counting TPS
        $time = microtime(true);
        $arr_time = explode('.', $time, 2);
        $this->tps = [
            'time' => $arr_time['0'],
            'count' => 0
        ];
    }

    // Method for check connecntion and reconnect
    protected function connect()
    {
        try {
            while ( ! $this->getConnection() || ! $this->getChannel() ) {
                echo $this->cmd_output_services->setAlertMessage("Reconnect connect rabbitMQ", '🔥', 'WARNING');
                sleep(2);
            }

            echo $this->cmd_output_services->setAlertMessage('RabbitMQ Connected Success', '✅', 'SUCCESS');

            return true;
        } catch (Exception $e) {
            echo $this->cmd_output_services->setAlertMessage("Function Connect RabbitMQ (Rabbit Client)", '❌', 'FAIL');
            echo $this->cmd_output_services->setAlertMessage("EXCEPTION THROWN: " . $e->getMessage(), '', 'FAIL');

            return false;
        }
    }

    // Method for connect to rabbitMQ 
    private function getConnection()
    {
        // Get queue config
        $config_queue = $this->randomGetServers();

        echo $this->cmd_output_services->setAlertMessage("Attempt to connect to RabbitMQ : " . $config_queue['host'], '', '');

        try {
            // Connect queue
            $this->connection = new AMQPStreamConnection($config_queue['host'], $config_queue['port'], $config_queue['username'], $config_queue['password']);

            // Check connection status
            if ( $this->connection->isConnected() ) {
                echo $this->cmd_output_services->setAlertMessage("RabbitMQ Connected at {$config_queue['host']}", '', 'SUCCESS');
            } else {
                echo $this->cmd_output_services->setAlertMessage("Cannot Connected RabbitMQ at {$config_queue['host']}", '❌', 'FAIL');
            }

            return true;
        } catch (Exception $e) {
            echo $this->cmd_output_services->setAlertMessage("Function Get Connect RabbitMQ (Rabbit Client)", '❌', 'FAIL');
            echo $this->cmd_output_services->setAlertMessage("EXCEPTION THROWN: " . $e->getMessage(), '', 'FAIL');

            return false;
        }
    }
    
    private function getChannel()
    {
        try {
            if (empty($this->connection)) {
                echo $this->cmd_output_services->setAlertMessage("Function Get Channel (Rabbit Client)", '❌', 'FAIL');
                echo $this->cmd_output_services->setAlertMessage("Connenction is Empty !!!", '', 'FAIL');
                return false;
            }
        
            $this->channel = $this->connection->channel();

            return true;
        } catch (Exception $e) {
            echo $this->cmd_output_services->setAlertMessage("Function Get Channel (Rabbit Client)", '❌', 'FAIL');
            echo $this->cmd_output_services->setAlertMessage("EXCEPTION THROWN: " . $e->getMessage(), '', 'FAIL');

            return false;
        }
    }

    private function setQueueDeclare($queue_name)
    {
        if (empty($this->channel)) {
            echo $this->cmd_output_services->setAlertMessage("Function Set Queue Declare (Rabbit Client)", '❌', 'FAIL');
            echo $this->cmd_output_services->setAlertMessage("Channel is Empty !!!", '', 'FAIL');
            return false;
        }

        $this->channel->queue_declare($queue_name, false, false, false, false);

        return true;
    }

    // Method for get servers
    private function randomGetServers()
    {
        $servers = $this->conf_queue['rabbit']['servers'];
        // Get count of server
        $total = count($servers);

        // Define index
        $index = 0;

        if ($total > 1) {
            // Random index
            $index = rand(0, ($total - 1));
        }

        return $servers[$index];
    }

    // Method for disconnect rabbitMQ
    protected function disconnect()
    {
        try {
            $this->channel->close();
            $this->connection->close();

            unset (
                $this->channel,
                $this->connection
            );

            echo $this->cmd_output_services->setAlertMessage('RabbitMQ disconnected', '', 'INFO');

            return true;
        } catch (Exception $e) {
            echo $this->cmd_output_services->setAlertMessage('Function disconnect (Rabbit Client)', '❌', 'FAIL', true);
            echo $this->cmd_output_services->setAlertMessage('EXCEPTION THROWN: ' . $e->getMessage(), '', 'FAIL');

            return false;
        }
    }

    public function startWorker($queue_name)
    {
        // Connect queue
        $this->connect();
            
        if ($this->connection && $this->connection->isConnected()) {
            echo $this->cmd_output_services->setAlertMessage("Waiting for job ({$queue_name}) ...", '🔥', 'WARNING');

            try {
                while (true) {
                    $this->workerWork($queue_name);
                }
            } catch (Exception $e) {
                echo $this->cmd_output_services->setAlertMessage('Function Start Worker (Rabbit Worker)', '❌', 'FAIL', true);
                echo $this->cmd_output_services->setAlertMessage('EXCEPTION THROWN: ' . $e->getMessage(), '', 'FAIL');
                exit;
            }
        }
    }

    private function workerWork($queue_name)
    {
        // Queue Declare
        $this->setQueueDeclare($queue_name);

        try {
            // Recived message
            $callback = function ($msg) {
                $this->setCallbackFn($msg->delivery_info['routing_key'], $msg->body);
            };

            $this->channel->basic_consume($queue_name, '', false, true, false, false, $callback);
            
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
        } catch (Exception $e) {
            echo $this->cmd_output_services->setAlertMessage('Function Start Worker (Rabbit Worker)', '❌', 'FAIL', true);
            echo $this->cmd_output_services->setAlertMessage('EXCEPTION THROWN: ' . $e->getMessage(), '', 'FAIL');
            exit;
        }
        
        $this->disconnect();
    }

    // Method for check transaction per second
    private function checkTps()
    {
        if (!defined('TPS')) {
            return;
        }

        $tps = TPS;

        $time = microtime(true);
        $arr_time = explode('.', $time, 2);

        // Setting count
        if ($this->tps['time'] === $arr_time['0']) {
            ++$this->tps['count'];
        } else {
            // reset count
            $tmp_time = explode('.', $time);
            $this->tps['time'] = $tmp_time['0'];
            $this->tps['count'] = 1;
        }

        // Check max transaction
        if ($this->tps['count'] > $tps) {
            $sleep = 10000 -$arr_time['1']; // milisec
            usleep($sleep);

            echo "🔥  Max transaction (" . $tps . " TPS) waitting {$sleep} milisecond...\n";
        }
    }

    protected function setCallbackFn($routing_key, $job)
    {
        echo "Worker Running\n";
        sleep(3);
    }
}
