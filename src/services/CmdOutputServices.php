<?php

namespace EggDigital\Worker\Services;

class CmdOutputServices
{
	public function setAlertMessage($text, $icon = null, $status = null, $border = false)
	{
		$msg = ($border) ? $this->colorize("==========================================================\n", $status) : '';
		$msg .= (!empty($icon)) ? "{$icon}  " : '';
		$msg .= (!empty($status)) ? $this->colorize($text, $status) : $text;
		$msg .= ($border) ? $this->colorize("\n==========================================================\n", $status) : "\n";
		return $msg;
	}

	private function colorize($text, $status)
	{
		$out = '';
		switch($status) {
			case 'SUCCESS': $out = "[32m"; break; // Green
			case 'FAIL':    $out = "[31m"; break; // Red
			case 'WARNING': $out = "[33m"; break; // Yellow
			case 'INFO':    $out = "[34m"; break; // Blue
			default: throw new Exception("Invalid status: {$status}");
		}
		return chr(27)."{$out}{$text}".chr(27)."[0m";
	}
}
